<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Author;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Author::factory(20)->create();

        Book::each(function ($book) {
            $author_ids = Author::inRandomOrder()
                ->limit(rand(1, 5))
                ->pluck('id');

            $book->authors()->sync($author_ids);
        });


        // Seeder untuk create author_id dan masukkan dalam table books
        // ---> case 1 to many relationship
        // Book::each(function ($book) {
        //     $book->author_id = rand(1, 30);
        //     $book->save();
        // });
    }
}