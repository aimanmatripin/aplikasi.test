<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/book/create', [BookController::class, 'create'])->name('book-create');
Route::get('/book/{id}', [BookController::class, 'show'])->name('book-single');
Route::get('/book/{id}/edit', [BookController::class, 'edit'])->name('book-edit');
Route::post('/book/{id}/edit', [BookController::class, 'update'])->name('book-update');
Route::delete('/book/{id}', [BookController::class, 'destroy'])->name('book-destroy');
Route::get('/books', [BookController::class, 'index'])->name('book-listing');
Route::post('/book', [BookController::class, 'store'])->name('book-store');
Route::get('/authors', [BookController::class, 'authors'])->name('author-listing');
Route::get('/author/{id}', [BookController::class, 'author'])->name('author-single');

Route::get('/test', [DashboardController::class, 'test']);


// redirect Route

Route::redirect('/aiman', '/');

// Direct view tanpa controller

Route::view(
    '/pelajar',
    'pelajar',
    [
        'nama' => 'Aiman'
    ]
);

/**
 * GET
 * POST
 * PUT
 * PATCH
 * DELETE
 * OPTIONS
 * 
 */

Route::get('/kelas', function () {
    return view('go');
});

Route::post('/kelas', function () {
    echo "<h1>POST --- Kelas Programming</h1>";
});

Route::put('kelas', function () {

    // Your function here 
});

Route::patch('kelas', function () {

    // Your function here 
});

Route::delete('kelas', function () {

    // Your function here 
});

Route::options('kelas', function () {

    // Your function here 
});


//parameter

Route::get('/welcome/{name?}', function ($name = 'Saudara / Saudari') {
    echo "<h1>Selamat datang, $name </h1>";
});


// model binding

Route::resource('user', UserController::class);