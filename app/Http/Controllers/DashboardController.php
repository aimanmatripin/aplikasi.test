<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function test()
    {


        // 1. using simple array
        // $books = DB::select('select * from books where id = ?', [2]);

        //2. using associative array
        /* 
        $books = DB::select(
            'select * from books where id = :id',
            [
                'id' => 77
            ]
        );
 */
        /* 
        $books = DB::insert(
            'insert into books (title, synopsis, price)
            values (?, ?, ?)',
            [
                'Puteri Gunung Ledang',
                'Puteri Gunung Ledang yang suka makan suka makan suka tinggal di dalam gunung ledang',
                '129.99',

            ]
        );
 */

        $select = DB::select(
            'select * from books order by id desc limit 1'
        );

        // dd($books);
        dd($select);
    }
}