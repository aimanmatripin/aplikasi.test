<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Author;
use Illuminate\Http\Request;

class BookController extends Controller
{
    //

    public function index()
    {
        $books = Book::select(['id', 'title', 'price'])
            ->with('authors')
            ->paginate(10);

        return view('books.listing', ['books' => $books]);
        // dd($books);
    }

    public function show($id)
    {
        $book = Book::with('authors')->find($id);
        return view('books.single', ['book' => $book]);
        //dd($book);

        // echo "<strong>Title</strong> : " . $book->title . "<br>";
        // echo "<strong>Price</strong> : " . $book->price . "<br>";
        // echo "<strong>Synopsis</strong> : " . nl2br($book->synopsis) . "";
    }

    public function authors()
    {
        $authors = Author::with('books')->get();

        return view('books.authors', [
            'authors' =>  $authors
        ]);
    }

    public function author($id)
    {
        $author = Author::with('books')->find($id);

        return view('books.author-single', [
            'author' =>  $author
        ]);
    }

    public function edit($id)
    {
        $book = Book::findOrFail($id); // findOrFail() - untuk cari id yg wujud sahaja.
        // $book = Book::find($id); // dind() - display page edit walaupun id tidak wujud

        // echo "Book editing form here";

        return view('books.edit', ['book' => $book]);
    }

    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        //dd($request);

        $validated_data = $request->validate([
            'title' => 'required|min:5|max:255',
            'price' => 'required|numeric',
            'synopsis' => 'required|min:50|max:1000',

        ]);

        $book->title = $request->title;
        $book->price = $request->price;
        $book->synopsis = $request->synopsis;

        $book->save();

        return back()->with('success', 'Book has been updated');
    }

    public function create()
    {
        return view('books.create');
    }

    public function store(Request $request)
    {

        $validated_data = $request->validate([
            'title' => 'required|min:5|max:255',
            'price' => 'required|numeric',
            'synopsis' => 'required|min:50|max:1000',

        ]);

        $book = Book::create($validated_data);

        // $book->save();

        return redirect()->route('book-listing')->with('success', 'Your book has been added!');
    }

    public function destroy($id)
    {
        $book = Book::findOrFail($id);

        $book->delete();

        return redirect()->route('book-listing')->with('delete', 'Your book has been deleted!');
    }
}