@extends('layout.main')

@section('content')
<div class="row mt-5 mb-5">

    @if( session('success') )
    <p class="alert alert-success">{{ session('success') }}</p>

    @elseif ( session ('delete') )
    <p class="alert alert-danger">{{ session('delete') }}</p>
    @endif
    <div class="col">

        <div class="d-sm-flex justify-content-between mb-3">
            <h3>Authors Listing</h3>

            <a href="#" class="btn btn-primary btn-sm">Add author</a>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th colspan="2">Name</td>
                </tr>
            </thead>

            @foreach($authors as $author)
            <tr>
                <td colspan="2"><strong>Book by {{ $author->name }}</strong> </td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td>
                    <ol>
                        @foreach($author->books as $book)
                        <li><a href="{{ route( 'book-single' , $book->id ) }}">{{ $book->title }}</a> </li>
                        @endforeach
                    </ol>

                    @if ( $author->books->count() < 1 ) <em>Author does not have any books listed here.</em>
                        @endif
                </td>

            </tr>
            @endforeach
        </table>


    </div>
</div>

@endsection