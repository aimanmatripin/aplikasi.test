@extends('layout.main')

@section('content')
<div class="row mt-5 mb-5">

    @if( session('success') )
    <p class="alert alert-success">{{ session('success') }}</p>

    @elseif ( session ('delete') )
    <p class="alert alert-danger">{{ session('delete') }}</p>
    @endif
    <div class="col">

        <div class="d-sm-flex justify-content-between mb-3">
            <h3>Books Listing</h3>

            <a href="{{ route('book-create') }}" class="btn btn-primary btn-sm">Add book</a>
        </div>

        <table class="table table-striped">
            <tr>
                <td><strong>ID</strong></td>
                <td><strong>TITLE</strong></td>
                <td><strong>AUTHORS</strong></td>
                <td><strong>PRICE</strong></td>
                <td><strong>ACTION</strong></td>
            </tr>
            @foreach($books as $book)
            <tr>
                <td>{{ $book->id }}</td>
                <td><a href="{{ route('book-single', $book->id) }}">{{ $book->title }}</a></td>
                <td>
                    <ol>
                        @foreach( $book->authors as $author )
                        <li>
                            <a href="{{ route( 'author-single', $author->id ) }}">{{ $author->name }}</a>
                        </li>
                        @endforeach
                    </ol>

                </td>
                <td>RM{{ $book->price }}</td>
                <td><a class="btn btn-outline-primary btn-sm text-nowrap"
                        href="{{ route('book-edit', $book->id) }}">EDIT</a>
                    <form action="{{ route('book-destroy', $book->id) }}" method="post"
                        onsubmit="return confirm('Are you sure to delete book titled {{ $book->title }}')"
                        class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="btn btn-outline-danger btn-sm text-nowrap" type="submit">DELETE</button>
                    </form>

                </td>
            </tr>
            @endforeach
        </table>

        {{ $books->links() }}
    </div>
</div>

@endsection