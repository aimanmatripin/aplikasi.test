@extends('layout.main')

@section('content')

<div class="row p-3">
    <h3>{{ $book->title }}</h3>
</div>
<div class="row">




    <div class="col-2"><img src="https://via.placeholder.com/200x300.png" alt=""></div>
    <div class="col-10">

        <h5>Price</h5>
        <p>RM{{ $book->price }}</p>

        <h5>Synopsis</h5>
        <p>{!! nl2br($book->synopsis) !!}</p>

        <h5>Authors</h5>
        <ol>
            @foreach ( $book->authors as $author )
            <li><a href="{{ route('author-single', $author->id) }}"> {{ $author->name }}</a></li>
            @endforeach
        </ol>
    </div>
</div>

@endsection