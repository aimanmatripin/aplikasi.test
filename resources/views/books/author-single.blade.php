@extends('layout.main')

@section('content')
<div class="row mt-5 mb-5">

    <div class="col">

        <div class="d-sm-flex justify-content-between mb-3">
            <h4>{{ $author->name }}</h4>
        </div>

        <table class="table table-striped">



            <tr>
                <td colspan="2"><strong>Book by {{ $author->name }}</strong> </td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td>

                    @if ( $author->books->count() < 1 ) <em>Author does not have any books listed here.</em>
                        @else
                        <ol>
                            @foreach($author->books as $book)
                            <li><a href="{{ route( 'book-single' , $book->id ) }}">{{ $book->title }}</a> </li>
                            @endforeach
                        </ol>
                        @endif
                </td>

            </tr>

        </table>


    </div>
</div>

@endsection