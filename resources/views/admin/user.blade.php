@extends ('layout.main')

@section('content')
<div class="row mt-5">
    <div class="col-8">
        <h2>User Information</h2>

        @forelse($users as $user)

        <p><strong>Name:</strong> {{ $user['name'] }}</p>

        <p><strong>Email:</strong> {{ $user['email'] }}</p>

        <p><strong>Course:</strong> {{ $user['course'] }}</p>

        <hr>


        @empty

        <h4>no record found</h4>

        @endforelse

        {!! $html_content !!}

    </div>

    <div class="col-4 bg-light p-3">
        @include('layout.sidebar')
    </div>
</div>


@endsection